package assignments.assignment3;

public class Mahasiswa extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;
    private int banyakMatkul = 0;       // menyimpan banyak matkul yang diikuti

    public Mahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        super("Mahasiswa", nama);
        this.npm = npm;
    }

    public void addMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        for(int i = 0; i < banyakMatkul; i++) 
            if (daftarMataKuliah[i] == mataKuliah) {        // jika sudah diambil
                System.out.printf("[DITOLAK] %s telah diambil sebelumnya\n", mataKuliah.getNama());
                return;
            }
        if (mataKuliah.getKapasitas() - mataKuliah.getBanyakMahasiswa() == 0) {     // jika sudah penuh kapasitasnya
            System.out.printf("[DITOLAK] %s telah penuh kapasitasnya\n", mataKuliah.getNama());
            return;
        }
        System.out.printf("%s berhasil menambahkan mata kuliah %s\n", this.getNama(), mataKuliah.getNama());
        daftarMataKuliah[banyakMatkul] = mataKuliah;
        banyakMatkul++;
        mataKuliah.addMahasiswa(this);
    }

    public void dropMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        int terhapus = 0;
        for (int i = 0; i < banyakMatkul; i++) {
            if (daftarMataKuliah[i] == mataKuliah) {    // jika ketemu
                terhapus = 1;
                System.out.printf("%s berhasil drop mata kuliah %s\n", this.getNama(), mataKuliah.getNama());
                for (int j = i; j < banyakMatkul-1; j++)        // update isi daftar mata kuliah
                    daftarMataKuliah[j] = daftarMataKuliah[j+1];
                banyakMatkul--;
                mataKuliah.dropMahasiswa(this);
                break;
            }
        }
        if (terhapus == 0) {        // jika matkul belum diambil
            System.out.printf("[DITOLAK] %s belum pernah diambil\n", mataKuliah.getNama());
        }
    }

    public String extractTanggalLahir() {
        /* TODO: implementasikan kode Anda di sini */
        String npmString = String.format("%d",this.npm);
        //                              /          Hari        /  /         Bulan        /      /   Tahun   /
        return String.format("%s-%s-%d",npmString.substring(4,6), npmString.substring(6,8), (long)(npm/100)%10000);
    }

    public String extractJurusan() {
        /* TODO: implementasikan kode Anda di sini */
        return String.format("%d",this.npm).substring(2,4).equals("01") ? "Ilmu Komputer" : "Sistem Informasi";
    }

    //setter-getter
    public MataKuliah[] getDaftarMataKuliah() {
        return this.daftarMataKuliah;
    }

    public int getBanyakMatkul() {
        return this.banyakMatkul;
    }

}
