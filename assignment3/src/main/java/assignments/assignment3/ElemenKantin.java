package assignments.assignment3;

public class ElemenKantin extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private Makanan[] daftarMakanan = new Makanan[10];
    private int banyakMakanan = 0;

    public ElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super("ElemenKantin", nama);
    }

    public void setMakanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        for(int i = 0; i < banyakMakanan; i++) {
            if (nama == daftarMakanan[i].getNama()) {       // jika makanan sudah pernah terdaftar
                System.out.printf("[DITOLAK] %s sudah pernah terdaftar\n", nama);
                return;
            }
        }
        System.out.printf("%s telah mendaftarkan makanan %s dengan harga %d\n", this.getNama(), nama, harga);
        daftarMakanan[banyakMakanan] = new Makanan(nama, harga);
        banyakMakanan++;
    }

    //setter-getter
    public int getBanyakMakanan() {
        return this.banyakMakanan;
    }

    public Makanan[] getDaftarMakanan() {
        return this.daftarMakanan;
    }
}
