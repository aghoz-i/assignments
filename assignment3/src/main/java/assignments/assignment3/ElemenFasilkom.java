package assignments.assignment3;

public class ElemenFasilkom implements Comparable < ElemenFasilkom > {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String tipe;
    
    private String nama;

    private int friendship = 0;

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    private int banyakDisapa = 0;   // menyimpan banyak orang yang sudah disapa

    public ElemenFasilkom(String tipe, String nama) {
        super();
        this.tipe = tipe;
        this.nama = nama;
    }

    public void menyapa(ElemenFasilkom elemenFasilkom) {
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0; i < banyakDisapa; i++) {
            if (telahMenyapa[i] == elemenFasilkom) {        // jika sudah menyapa
                System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", this.nama, elemenFasilkom.getNama());
                return;                 // keluar method langsung
            }
        }
        // update menyapa di this
        telahMenyapa[banyakDisapa] = elemenFasilkom;
        banyakDisapa++;
        // update menyapa di elemen satunya
        ElemenFasilkom[] temanTelahMenyapa = elemenFasilkom.getTelahMenyapa();
        temanTelahMenyapa[elemenFasilkom.getBanyakDisapa()] = this;
        elemenFasilkom.setTelahMenyapa(temanTelahMenyapa);
        elemenFasilkom.setBanyakDisapa(elemenFasilkom.getBanyakDisapa()+1);
        System.out.printf("%s menyapa dengan %s\n", this.nama, elemenFasilkom.getNama());
    }

    public void resetMenyapa() {
        /* TODO: implementasikan kode Anda di sini */
        banyakDisapa = 0;
    }

    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    // setter-getter
    public String getNama() {
        return this.nama;       
    }

    public String getTipe() {
        return this.tipe;
    }

    public int getFriendship() {
        return this.friendship;
    }

    public int getBanyakDisapa() {
        return this.banyakDisapa;
    }

    public void setBanyakDisapa(int banyakDisapa) {
        this.banyakDisapa = banyakDisapa;
    }

    public void setTelahMenyapa(ElemenFasilkom[] telahMenyapa) {
        this.telahMenyapa = telahMenyapa;
    }

    public ElemenFasilkom[] getTelahMenyapa() {
        return this.telahMenyapa;
    }

    public void setFriendship(int friendship) {
        this.friendship = friendship;
    }

    // override comparable
    @Override
    public int compareTo(ElemenFasilkom other) {
        if (this.getFriendship() == other.getFriendship()) {    // apabila sama sort berdasar nama
            return this.getNama().compareTo(other.getNama());
        } else {    // sort berdasar friendship
            return other.getFriendship() - this.getFriendship();
        }
    }
}
