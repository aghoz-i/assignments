package assignments.assignment3;

import java.util.Scanner;
import java.util.Arrays;

public class Main {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    private static int totalMataKuliah = 0;

    private static int totalElemenFasilkom = 0; 

    public static ElemenFasilkom cariObjek(String objek) {     // method untuk mencari elemen di daftar berdasarkan nama elemen
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarElemenFasilkom[i].getNama().equals(objek)) {
                return daftarElemenFasilkom[i];
            }
        }
        return daftarElemenFasilkom[0];          // dumb return
    }

    public static MataKuliah cariMatkul(String namaMataKuliah) {   // method untuk mencari matkul di daftar berdasarkan nama matkul
        for (int i = 0; i < totalMataKuliah; i++) {
            if (daftarMataKuliah[i].getNama().equals(namaMataKuliah)) {
                return daftarMataKuliah[i];
            }
        }
        return daftarMataKuliah[0];          // dumb return
    }

    public static void addMahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        daftarElemenFasilkom[totalElemenFasilkom] = new Mahasiswa(nama, npm);
        totalElemenFasilkom++;
        System.out.printf("%s berhasil ditambahkan\n", nama);
    }

    public static void addDosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        daftarElemenFasilkom[totalElemenFasilkom] = (Dosen) new Dosen(nama);
        totalElemenFasilkom++;
        System.out.printf("%s berhasil ditambahkan\n", nama);
    }

    public static void addElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        daftarElemenFasilkom[totalElemenFasilkom] = (ElemenKantin) new ElemenKantin(nama);
        totalElemenFasilkom++;
        System.out.printf("%s berhasil ditambahkan\n", nama);
    }

    public static void menyapa(String objek1, String objek2) {
        /* TODO: implementasikan kode Anda di sini */
        if (objek1.equals(objek2)) {        // jika merupakan objek yang sama
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
            return;
        } 
        ElemenFasilkom orang1, orang2;
        // cari objek yang sesuai dengan nama masing-masing
        // for(int i = 0; i < totalElemenFasilkom; i++) {  
        //     if (daftarElemenFasilkom[i].getNama().equals(orang1)) {
        //         orang1 = daftarElemenFasilkom[i];
        //     } else if (daftarElemenFasilkom[i].getNama().equals(orang2)) {
        //         orang2 = daftarElemenFasilkom[i];
        //     }
        // }
        orang1 = cariObjek(objek1);
        orang2 = cariObjek(objek2);
        orang1.menyapa(orang2);
        if (orang1.getTipe().equals("Dosen") && orang2.getTipe().equals("Mahasiswa")) {
            MataKuliah[] daftarMatkul = ((Mahasiswa)orang2).getDaftarMataKuliah();
            for (int i = 0; i < ((Mahasiswa)orang2).getBanyakMatkul(); i++) {
                if (daftarMatkul[i] == ((Dosen)orang1).getMatkul()) {
                    orang1.setFriendship(Math.min(100, orang1.getFriendship()+2));
                    orang2.setFriendship(Math.min(100, orang2.getFriendship()+2));
                    break;
                }
            }
        } else if (orang1.getTipe().equals("Mahasiswa") && orang2.getTipe().equals("Dosen")) {
            MataKuliah[] daftarMatkul = ((Mahasiswa)orang1).getDaftarMataKuliah();
            for (int i = 0; i < ((Mahasiswa)orang1).getBanyakMatkul(); i++) {
                if (daftarMatkul[i] == ((Dosen)orang2).getMatkul()) {
                    orang1.setFriendship(Math.min(100, orang1.getFriendship()+2));
                    orang2.setFriendship(Math.min(100, orang2.getFriendship()+2));
                    break;
                }
            }
        }
    }

    public static void addMakanan(String objek, String namaMakanan, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom orang;
        orang = cariObjek(objek);
        if (!(orang.getTipe().equals("ElemenKantin"))) {      // jika objek bukan elemen kantin
            System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin\n", orang.getNama());
            return;
        }
        Makanan[] daftarMakanan = ((ElemenKantin)orang).getDaftarMakanan();
        for (int i = 0; i < ((ElemenKantin)orang).getBanyakMakanan(); i++) {
            if (daftarMakanan[i].getNama().equals(namaMakanan)) {
                System.out.printf("[DITOLAK] %s sudah pernah terdaftar.\n", daftarMakanan[i].getNama());
                return;
            }
        }
        ((ElemenKantin)orang).setMakanan(namaMakanan, harga);
    }

    public static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom orang1, orang2;
        orang1 = cariObjek(objek1);
        orang2 = cariObjek(objek2);
        if (!(orang2.getTipe().equals("ElemenKantin"))) {        // jika objek2 bukan elemen kantin
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
            return;
        }
        if (objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
            return;
        }
        Makanan[] daftarMakanan = ((ElemenKantin)orang2).getDaftarMakanan();
        int terbeli = 0;
        for (int i = 0; i < ((ElemenKantin)orang2).getBanyakMakanan(); i++) {
            if (daftarMakanan[i].getNama().equals(namaMakanan)) {
                terbeli = 1;
                System.out.printf("%s berhasil membeli %s seharga %d\n", orang1.getNama(), daftarMakanan[i].getNama(), daftarMakanan[i].getHarga());
            }
        }
        if (terbeli == 0) {     // jika tidak dijual makanan yang dicari
            System.out.printf("[DITOLAK] %s tidak menjual %s\n", orang2.getNama(), namaMakanan);
            return;
        }
        orang1.setFriendship(Math.min(100, orang1.getFriendship()+1));
        orang2.setFriendship(Math.min(100, orang2.getFriendship()+1));
    }

    public static void createMatkul(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        daftarMataKuliah[totalMataKuliah] = new MataKuliah(nama, kapasitas);
        totalMataKuliah++;
        System.out.printf("%s berhasil ditambahkan dengan kapasitas %d\n", nama, kapasitas);
    }

    public static void addMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom orang;
        MataKuliah matkul;
        orang = cariObjek(objek);
        matkul = cariMatkul(namaMataKuliah);
        if (!(orang.getTipe().equals("Mahasiswa"))) {            // jika objek bukan mahasiswa
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
            return;
        }
        ((Mahasiswa)orang).addMatkul(matkul);
    }

    public static void dropMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom orang;
        MataKuliah matkul;
        orang = cariObjek(objek);
        matkul = cariMatkul(namaMataKuliah);
        if (!(orang.getTipe().equals("Mahasiswa"))) {            // jika objek bukan mahasiswa
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
            return;
        }
        ((Mahasiswa)orang).dropMatkul(matkul);
    }

    public static void mengajarMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom orang;
        MataKuliah matkul;
        orang = cariObjek(objek);
        matkul = cariMatkul(namaMataKuliah);
        if (!(orang.getTipe().equals("Dosen"))) {            // jika objek bukan dosen
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
            return;
        }
        ((Dosen)orang).mengajarMataKuliah(matkul);
    }

    public static void berhentiMengajar(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom orang;
        orang = cariObjek(objek);
        if (!(orang.getTipe().equals("Dosen"))) {            // jika objek bukan dosen
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
            return;
        }
        ((Dosen)orang).dropMataKuliah();
    }

    public static void ringkasanMahasiswa(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom orang;
        orang = cariObjek(objek);
        if (!(orang.getTipe().equals("Mahasiswa"))) {            // jika objek bukan mahasiswa
            System.out.printf("[DITOLAK] %s bukan merupakan seorang mahasiswa\n", orang.getNama());
            return;
        }
        Mahasiswa mahasiswa = (Mahasiswa)orang;
        System.out.printf("Nama: %s\n", mahasiswa.getNama());
        System.out.printf("Tanggal lahir: %s\n", mahasiswa.extractTanggalLahir());
        System.out.printf("Jurusan: %s\n", mahasiswa.extractJurusan());
        System.out.printf("Daftar Mata Kuliah:\n");
        if (mahasiswa.getBanyakMatkul() == 0) {       // jika belum ada matkul yang diambil
            System.out.println("Belum ada mata kuliah yang diambil");
            return;
        }
        MataKuliah[] daftarMatkul = mahasiswa.getDaftarMataKuliah();
        for (int i = 0; i < mahasiswa.getBanyakMatkul(); i++) {
            System.out.printf("%d. %s\n", i+1, daftarMatkul[i].getNama());
        }
    }

    public static void ringkasanMataKuliah(String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        MataKuliah matkul = cariMatkul(namaMataKuliah);
        System.out.printf("Nama mata kuliah: %s\n", matkul.getNama());
        System.out.printf("Jumlah mahasiswa: %d\n", matkul.getBanyakMahasiswa());
        System.out.printf("Kapasitas: %d\n", matkul.getKapasitas());
        System.out.printf("Dosen Pengajar: %s\n", matkul.getNamaDosen());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");
        if (matkul.getBanyakMahasiswa() == 0) {     // jika belum ada mahasiswa yang mengambil matkul ini
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
            return;
        }
        Mahasiswa[] daftarMahasiswa = matkul.getDaftarMahasiswa();
        for (int i = 0; i < matkul.getBanyakMahasiswa(); i++) {
            System.out.printf("%d. %s\n", i+1, daftarMahasiswa[i].getNama());
        }
    }

    public static void nextDay() {
        /* TODO: implementasikan kode Anda di sini */
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        for (int i = 0; i < totalElemenFasilkom; i++) {
            int friendship = daftarElemenFasilkom[i].getFriendship();
            int banyakDisapa = daftarElemenFasilkom[i].getBanyakDisapa();
            if (banyakDisapa*2 >= totalElemenFasilkom-1) {  // jika menyapa lebih dari setengah elemen
                friendship = Math.min(friendship+10, 100);
            } else {        // jika tidak
                friendship = Math.max(friendship-5, 0);
            }
            daftarElemenFasilkom[i].setFriendship(friendship);  // update nilai friendship elemen Fasilkom
            daftarElemenFasilkom[i].resetMenyapa();         // reset array menyapa setiap elemen
        }
        friendshipRanking();
    }

    public static void friendshipRanking() {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom[] daftarElemenSekarang = Arrays.copyOfRange(daftarElemenFasilkom, 0, totalElemenFasilkom);   // copy elemen array yang bukan berisi null
        Arrays.sort(daftarElemenSekarang);  // sort array hasil copy
        for (int i = 0; i < totalElemenFasilkom; i++) { //cetak berdasar hasil sort
            System.out.printf("%d. %s(%d)\n", i+1, daftarElemenSekarang[i].getNama(), daftarElemenSekarang[i].getFriendship());
        }
    }

    public static void programEnd() {
        /* TODO: implementasikan kode Anda di sini */
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}
