package assignments.assignment3;

public class MataKuliah {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;
    private int banyakMahasiswa = 0;        //menyimpan banyak mahasiswa

    public MataKuliah(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.kapasitas = kapasitas;
        daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        daftarMahasiswa[banyakMahasiswa] = mahasiswa;
        banyakMahasiswa++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0; i < banyakMahasiswa; i++) {
            if (daftarMahasiswa[i] == mahasiswa) {
                for (int j = i; j < banyakMahasiswa-1; j++) 
                    daftarMahasiswa[j] = daftarMahasiswa[j+1];
                banyakMahasiswa--;
                break;
            }
        }
    }

    public void addDosen(Dosen dosen) {
        /* TODO: implementasikan kode Anda di sini */
        this.dosen = dosen;
    }

    public void dropDosen() {
        /* TODO: implementasikan kode Anda di sini */
        this.dosen = null;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    public String getNamaDosen() {
        return this.dosen == null ? "Belum ada" : (this.dosen).getNama();
    }

    // setter-getter
    public String getNama() {
        return this.nama;
    }

    public int getKapasitas() {
        return this.kapasitas;
    }

    public int getBanyakMahasiswa() {
        return this.banyakMahasiswa;
    }

    public Dosen getDosen() {
        return this.dosen;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

}
