package assignments.assignment3;

public class Makanan {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;

    private long harga;

    public Makanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.harga = harga;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    // setter-getter
    public String getNama() {
        return this.nama;
    }

    public long getHarga() {
        return this.harga;
    }
}
