package assignments.assignment3;

public class Dosen extends ElemenFasilkom {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah mataKuliah;

    public Dosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super("Dosen", nama);
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */     
        if (this.mataKuliah != null) {          // jika sudah mengajar suatu matkul lain
            System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s\n", this.getNama(), this.mataKuliah.getNama());
            return;
        }
        if (mataKuliah.getDosen() != null) {                                 // jika matkul sudah memiliki dosen pengajar
            System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar\n", mataKuliah.getNama());
            return;
        }
        this.mataKuliah = mataKuliah;
        mataKuliah.addDosen(this);
        System.out.printf("%s mengajar mata kuliah %s\n", this.getNama(), mataKuliah.getNama());
    }

    public void dropMataKuliah() {
        /* TODO: implementasikan kode Anda di sini */
        if (this.mataKuliah == null) {          // jika sedang tidak mengajar matkul apapun
            System.out.printf("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun\n", this.getNama());
            return;
        }
        System.out.printf("%s berhenti mengajar %s\n", this.getNama(), this.mataKuliah.getNama());
        mataKuliah.dropDosen();
        this.mataKuliah = null;
    }

    //setter-getter
    public MataKuliah getMatkul() {
        return this.mataKuliah;
    }

}
