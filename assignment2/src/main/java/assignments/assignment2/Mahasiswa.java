package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;
    private int banyakMatkul = 0;
    private int banyakMasalah = 0;

    public Mahasiswa(String nama, long npm){
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.npm = npm;
        jurusan = String.format("%d",npm).substring(2,4).equals("01") ? "IK" : "SI";    // set jurusan
    }
    
    public void addMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        // cek validasi nama matkul
        for (int i = 0; i < banyakMatkul; i++) {
            if (this.mataKuliah[i].getNama().equals(mataKuliah.getNama())) {
                System.out.printf("[DITOLAK] %s telah diambil sebelumnya\n", mataKuliah.getNama());
                return;
            }
        }
        // cek validasi slot tersisa matkul
        if (mataKuliah.getBanyakMahasiswa() == mataKuliah.getKapasitas()) {
            System.out.printf("[DITOLAK] %s telah penuh kapasitasnya\n", mataKuliah.getNama());
            return;
        }
        // cek validasi banyak matkul
        if (banyakMatkul == 10) {System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10."); return;}
        
        this.mataKuliah[banyakMatkul] = mataKuliah;         // masukkan ke arrray
        banyakMatkul++;                                     // tambahkan jumlah matkul
        mataKuliah.addMahasiswa(this);              // update di matakuliah
    }

    public void dropMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        int terdrop = 0;            // tanda ada yang terdrop atau tidak
        for (int i = 0; i < banyakMatkul; i++)
            if (this.mataKuliah[i] == mataKuliah) {                 // jika di index i adalah matkul yang didrop
                terdrop = 1;
                for (int j = i; j < banyakMatkul-1; j++)
                    this.mataKuliah[j] = this.mataKuliah[j+1];      // geser isi dari array di belakang index i
            }
        if (terdrop == 0) {     // jika belum pernah mengambil matkul
            System.out.printf("[DITOLAK] %s belum pernah diambil\n", mataKuliah.getNama());
            return;
        }
        banyakMatkul--;                                             // kurangi jumlah matkul
        mataKuliah.dropMahasiswa(this);             // update di matakuliah
    }


    public void cekIRS(){
        /* TODO: implementasikan kode Anda di sini */
        masalahIRS = new String[11];        // reset masalah
        totalSKS = 0;                       // reset totalSks
        banyakMasalah = 0;                  // reset banyak masalah
        for (int i = 0; i < banyakMatkul; i++) 
            totalSKS += mataKuliah[i].getSKS();     // hitung total SKS
        if (totalSKS > 24) {                // jika sks > 24 tambahkan ke masalah
            masalahIRS[banyakMasalah] = String.format("SKS yang Anda ambil lebih dari 24");
            banyakMasalah++;
        }
        for (int i = 0; i < banyakMatkul; i++) {
            String namaMatkul = mataKuliah[i].getNama();        // menyimpan nama matkul
            String kode = mataKuliah[i].getKode();              // menyimpan kode matkul
            if (!kode.equals("CS") && !kode.equals(jurusan)) {  // jika bermasalah tambahkan
                masalahIRS[banyakMasalah] = String.format("Mata Kuliah %s tidak dapat diambil di jurusan %s", namaMatkul, jurusan);
                banyakMasalah++;                                // tambahkan banyak masalah
            }
        }
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return nama;
    }

    public String getNama() {
        return nama;
    }

    public long getNpm() {
        return npm;
    }

    public int getBanyakMatkul() {
        return banyakMatkul;
    }

    public String getJurusan() {
        return jurusan.equals("IK") ? "Ilmu Komputer" : "Sistem Informasi";
    }

    public MataKuliah[] getMataKuliah() {
        return mataKuliah;
    }

    public int getSKS() {
        return totalSKS;
    }

    public int getBanyakMasalah() {
        return banyakMasalah;
    }

    public String[] getMasalahIRS() {
        return masalahIRS;
    }
}
