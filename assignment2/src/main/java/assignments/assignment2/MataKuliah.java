package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int banyakMahasiswa = 0;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        /* TODO: implementasikan kode Anda di sini */
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        daftarMahasiswa = new Mahasiswa[kapasitas];
    }


    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        daftarMahasiswa[banyakMahasiswa] = mahasiswa;   // masukkan ke array
        banyakMahasiswa++;                              // tambahkan banyak mahasiswa
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0; i < banyakMahasiswa; i++)
            if (daftarMahasiswa[i] == mahasiswa)                    // jika di index i adalah mahasiswa yang didrop
                for (int j = i; j < banyakMahasiswa-1; j++)
                    daftarMahasiswa[j] = daftarMahasiswa[j+1];      // geser isi dari array di belakang index i
        banyakMahasiswa--;                                          // kurangi jumlah mahasiswa
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return nama;
    }

    public String getKode() {
        return kode;
    }

    public int getSKS() {
        return sks;
    }

    public String getNama() {
        return nama;
    }

    public int getBanyakMahasiswa() {
        return banyakMahasiswa;
    }

    public int getKapasitas() {
        return kapasitas;
    }

    public Mahasiswa[] getMahasiswa() {
        return daftarMahasiswa;
    }
}
