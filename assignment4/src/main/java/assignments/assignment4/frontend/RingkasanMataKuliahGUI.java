package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {
    private JPanel panel;
    private JLabel titleLabel;
    private JLabel pilihMatkul;
    private JComboBox pilihMatkulDropdown;
    private JButton lihat;
    private JButton kembali;

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Ringkasan Mata Kuliah
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(Box.createRigidArea(new Dimension(0,10)));

        titleLabel = new JLabel("Ringkasan Mata Kuliah");
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(titleLabel);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        pilihMatkul = new JLabel("Pilih Nama Matkul");
        pilihMatkul.setFont(SistemAkademikGUI.fontGeneral);
        pilihMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(pilihMatkul);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        pilihMatkulDropdown = new JComboBox(new DefaultComboBoxModel(getDaftarMatkulUnefficentlySorted(daftarMataKuliah)));
        pilihMatkulDropdown.setFont(SistemAkademikGUI.fontGeneral);
        pilihMatkulDropdown.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihMatkulDropdown.setMaximumSize(new Dimension(200, 20));
        panel.add(pilihMatkulDropdown);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        lihat = new JButton("Lihat");
        lihat.setFont(SistemAkademikGUI.fontGeneral);
        lihat.setAlignmentX(Component.CENTER_ALIGNMENT);
        lihat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String teksMatkul = "" + pilihMatkulDropdown.getItemAt(pilihMatkulDropdown.getSelectedIndex());
                boolean sudah = false;
                if (teksMatkul.equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    sudah = true;
                } 
                if (!sudah) {
                    MataKuliah mataKuliah = getMataKuliah(teksMatkul, daftarMataKuliah);
                    frame.remove(panel);
                    frame.validate();
                    new DetailRingkasanMataKuliahGUI(frame, mataKuliah, daftarMahasiswa, daftarMataKuliah);
                }
            }
        });
        panel.add(lihat);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        kembali = new JButton("Kembali");
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(kembali);
        panel.add(Box.createRigidArea(new Dimension(0,10)));
        
        frame.add(panel);
        frame.validate();
        frame.repaint();
    }

    private String[] getDaftarMatkulUnefficentlySorted(ArrayList<MataKuliah> daftarMataKuliah) {
        int jumlahMatkul = daftarMataKuliah.size();
        if (jumlahMatkul == 0) {
            String[] rettt = {""};
            return rettt;
        }
        String[] ret = new String[jumlahMatkul];
        for (int i = 0; i < jumlahMatkul; i++) {
            MataKuliah mataKuliah = daftarMataKuliah.get(i);
            ret[i] = mataKuliah.getNama();
        }
        for (int i = 0; i < jumlahMatkul; i++) {
            for (int j = i+1; j < jumlahMatkul; j++) {
                if (ret[i].compareTo(ret[j]) >= 0) {
                    String temp = ret[i];
                    ret[i] = ret[j];
                    ret[j] = temp;
                }
            }
        }
        return ret;
    }
    // Uncomment method di bawah jika diperlukan
    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
}
