package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{
    private JPanel panel;
    private JLabel titleLabel;
    private JLabel kode;
    private JTextField kodeText;
    private JLabel nama;
    private JTextField namaText;
    private JLabel SKS;
    private JTextField SKSText;
    private JLabel kapasitas;
    private JTextField kapasitasText;
    private JButton tambahkan;
    private JButton kembali;

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // TODO: Implementasikan Tambah Mata Kuliah
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(Box.createRigidArea(new Dimension(0,10)));

        titleLabel = new JLabel("Tambah Mata Kuliah");
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(titleLabel);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        kode = new JLabel("Kode Mata Kuliah:");
        kode.setFont(SistemAkademikGUI.fontGeneral);
        kode.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(kode);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        kodeText = new JTextField(20);
        kodeText.setMaximumSize(kodeText.getPreferredSize());
        kodeText.setAlignmentX(Component.CENTER_ALIGNMENT);
        kodeText.setFont(SistemAkademikGUI.fontGeneral);
        kodeText.setBounds(0,0,200,30);
        panel.add(kodeText);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        nama = new JLabel("Nama Mata Kuliah:");
        nama.setFont(SistemAkademikGUI.fontGeneral);
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(nama);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        namaText = new JTextField(20);
        namaText.setMaximumSize(namaText.getPreferredSize());
        namaText.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaText.setFont(SistemAkademikGUI.fontGeneral);
        panel.add(namaText);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        SKS = new JLabel("SKS:");
        SKS.setFont(SistemAkademikGUI.fontGeneral);
        SKS.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(SKS);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        SKSText = new JTextField(20);
        SKSText.setMaximumSize(SKSText.getPreferredSize());
        SKSText.setAlignmentX(Component.CENTER_ALIGNMENT);
        SKSText.setFont(SistemAkademikGUI.fontGeneral);
        panel.add(SKSText);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        kapasitas = new JLabel("Kapasitas:");
        kapasitas.setFont(SistemAkademikGUI.fontGeneral);
        kapasitas.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(kapasitas);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        kapasitasText = new JTextField(20);
        kapasitasText.setMaximumSize(kapasitasText.getPreferredSize());
        kapasitasText.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitasText.setFont(SistemAkademikGUI.fontGeneral);
        panel.add(kapasitasText);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        tambahkan = new JButton("Tambahkan");
        tambahkan.setFont(SistemAkademikGUI.fontGeneral);
        tambahkan.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahkan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String teksKode = kodeText.getText();
                String teksNama = namaText.getText();
                String teksSKS = SKSText.getText();
                String teksKapasitas = kapasitasText.getText();
                boolean sudah = false;
                if (teksKode.equals("") || teksNama.equals("") || teksSKS.equals("") || teksKapasitas.equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    sudah = true;
                } else {
                    for (int i = 0; i < daftarMataKuliah.size(); i++) {
                        MataKuliah mataKuliah = daftarMataKuliah.get(i);
                        if ((mataKuliah.getNama()).equals(teksNama)) {
                            JOptionPane.showMessageDialog(frame, "Mata Kuliah "+teksNama+" sudah pernah ditambahkan sebelumnya");
                            sudah = true;
                            break;
                        }
                    }
                }
                if (!sudah) {
                    daftarMataKuliah.add(new MataKuliah(teksKode, teksNama, Integer.parseInt(teksSKS), Integer.parseInt(teksKapasitas)));
                    JOptionPane.showMessageDialog(frame, "Mata Kuliah "+teksNama+" berhasil ditambahkan");
                }
            }
        });
        panel.add(tambahkan);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        kembali = new JButton("Kembali");
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(kembali);
        panel.add(Box.createRigidArea(new Dimension(0,10)));
        
        frame.add(panel);
        frame.validate();
        frame.repaint();

    }
    
}
