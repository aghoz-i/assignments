package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{
    private JPanel panel;
    private JLabel titleLabel;
    private JLabel nama;
    private JTextField namaText;
    private JLabel NPM;
    private JTextField NPMText;
    private JButton tambahkan;
    private JButton kembali;

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
       
        // TODO: Implementasikan Tambah Mahasiswa
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(Box.createRigidArea(new Dimension(0,10)));

        titleLabel = new JLabel("Tambah Mahasiswa");
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(titleLabel);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        nama = new JLabel("Nama:");
        nama.setFont(SistemAkademikGUI.fontGeneral);
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(nama);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        namaText = new JTextField(20);
        namaText.setMaximumSize(namaText.getPreferredSize());
        namaText.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaText.setFont(SistemAkademikGUI.fontGeneral);
        namaText.setBounds(0,0,200,30);
        panel.add(namaText);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        NPM = new JLabel("NPM:");
        NPM.setFont(SistemAkademikGUI.fontGeneral);
        NPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(NPM);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        NPMText = new JTextField(20);
        NPMText.setMaximumSize(NPMText.getPreferredSize());
        NPMText.setAlignmentX(Component.CENTER_ALIGNMENT);
        NPMText.setFont(SistemAkademikGUI.fontGeneral);
        panel.add(NPMText);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        tambahkan = new JButton("Tambahkan");
        tambahkan.setFont(SistemAkademikGUI.fontGeneral);
        tambahkan.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahkan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String teksNama = namaText.getText();
                String teksNPM = NPMText.getText();
                Mahasiswa mahasiswa;
                boolean sudah = false;
                if (teksNama.equals("") || teksNPM.equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    sudah = true;
                } else {
                    for (int i = 0; i < daftarMahasiswa.size(); i++) {
                        mahasiswa = daftarMahasiswa.get(i);
                        if (String.format("%d",mahasiswa.getNpm()).equals(teksNPM)) {
                            JOptionPane.showMessageDialog(frame, "NPM "+teksNPM+" sudah pernah ditambahkan sebelumnya");
                            sudah = true;
                            break;
                        }
                    }
                }
                if (!sudah) {
                    JOptionPane.showMessageDialog(frame, "Mahasiswa "+teksNPM+"-"+teksNama+" berhasil ditambahkan");
                    daftarMahasiswa.add(new Mahasiswa(teksNama, Long.parseLong(teksNPM)));
                }
            }
        });
        panel.add(tambahkan);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        kembali = new JButton("Kembali");
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(kembali);
        panel.add(Box.createRigidArea(new Dimension(0,10)));
        
        frame.add(panel);
        frame.validate();
        frame.repaint();
    }
    
}
