package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HapusIRSGUI {
    private JPanel panel;
    private JLabel titleLabel;
    private JLabel pilihNPM;
    private JComboBox pilihNPMDropdown;
    private JLabel pilihMatkul;
    private JComboBox pilihMatkulDropdown;
    private JButton hapus;
    private JButton kembali;

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Hapus IRS
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(Box.createRigidArea(new Dimension(0,10)));

        titleLabel = new JLabel("Hapus IRS");
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(titleLabel);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        pilihNPM = new JLabel("Pilih NPM");
        pilihNPM.setFont(SistemAkademikGUI.fontGeneral);
        pilihNPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(pilihNPM);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        pilihNPMDropdown = new JComboBox(new DefaultComboBoxModel(getDaftarNPMUnefficientlySorted(daftarMahasiswa)));
        pilihNPMDropdown.setFont(SistemAkademikGUI.fontGeneral);
        pilihNPMDropdown.setMaximumSize(new Dimension(200, 20));
        pilihNPMDropdown.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(pilihNPMDropdown);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        pilihMatkul = new JLabel("Pilih Nama Matkul");
        pilihMatkul.setFont(SistemAkademikGUI.fontGeneral);
        pilihMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(pilihMatkul);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        pilihMatkulDropdown = new JComboBox(new DefaultComboBoxModel(getDaftarMatkulUnefficentlySorted(daftarMataKuliah)));
        pilihMatkulDropdown.setFont(SistemAkademikGUI.fontGeneral);
        pilihMatkulDropdown.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihMatkulDropdown.setMaximumSize(new Dimension(200, 20));
        panel.add(pilihMatkulDropdown);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        hapus = new JButton("Hapus");
        hapus.setFont(SistemAkademikGUI.fontGeneral);
        hapus.setAlignmentX(Component.CENTER_ALIGNMENT);
        hapus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String teksNPM = "" + pilihNPMDropdown.getItemAt(pilihNPMDropdown.getSelectedIndex());
                String teksNamaMatkul = "" + pilihMatkulDropdown.getItemAt(pilihMatkulDropdown.getSelectedIndex());
                boolean sudah = false;
                if (teksNPM.equals("") || teksNamaMatkul.equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    sudah = true;
                }
                if (!sudah) {
                    Mahasiswa mahasiswa = getMahasiswa(Long.parseLong(teksNPM), daftarMahasiswa);
                    MataKuliah mataKuliah = getMataKuliah(teksNamaMatkul, daftarMataKuliah);
                    JOptionPane.showMessageDialog(frame, mahasiswa.dropMatkul(mataKuliah));
                }
            }
        });
        panel.add(hapus);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        kembali = new JButton("Kembali");
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(kembali);
        panel.add(Box.createRigidArea(new Dimension(0,10)));
        
        frame.add(panel);
        frame.validate();
        frame.repaint();
    }

    private String[] getDaftarNPMUnefficientlySorted(ArrayList<Mahasiswa> daftarMahasiswa) {
        int jumlahMahasiswa = daftarMahasiswa.size();
        if (jumlahMahasiswa == 0) {
            String[] rettt = {""};
            return rettt;
        }
        String[] ret = new String[jumlahMahasiswa];
        for (int i = 0; i < jumlahMahasiswa; i++) {
            Mahasiswa mahasiswa = daftarMahasiswa.get(i);
            ret[i] = String.format("%d", mahasiswa.getNpm());
        }
        for (int i = 0; i < jumlahMahasiswa; i++) {
            for (int j = i+1; j < jumlahMahasiswa; j++) {
                if (ret[i].compareTo(ret[j]) >= 0) {
                    String temp = ret[i];
                    ret[i] = ret[j];
                    ret[j] = temp;
                }
            }
        }
        return ret;
    }

    private String[] getDaftarMatkulUnefficentlySorted(ArrayList<MataKuliah> daftarMataKuliah) {
        int jumlahMatkul = daftarMataKuliah.size();
        if (jumlahMatkul == 0) {
            String[] rettt = {""};
            return rettt;
        }
        String[] ret = new String[jumlahMatkul];
        for (int i = 0; i < jumlahMatkul; i++) {
            MataKuliah mataKuliah = daftarMataKuliah.get(i);
            ret[i] = mataKuliah.getNama();
        }
        for (int i = 0; i < jumlahMatkul; i++) {
            for (int j = i+1; j < jumlahMatkul; j++) {
                if (ret[i].compareTo(ret[j]) >= 0) {
                    String temp = ret[i];
                    ret[i] = ret[j];
                    ret[j] = temp;
                }
            }
        }
        return ret;
    }
    // Uncomment method di bawah jika diperlukan
    
    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
