package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    private JPanel panel;
    private JLabel titleLabel;
    private JLabel nama;
    private JLabel NPM;
    private JLabel jurusan;
    private JLabel[] daftarMatkul;
    private JLabel totalSKS;
    private JLabel cekIRS;
    private JLabel[] masalahIRS;
    private JButton selesai;

    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Detail Ringkasan Mahasiswa
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(Box.createRigidArea(new Dimension(0,10)));

        titleLabel = new JLabel("Detail Ringkasan Mahasiswa");
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(titleLabel);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        nama = new JLabel("Nama: "+mahasiswa.getNama());
        nama.setFont(SistemAkademikGUI.fontGeneral);
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(nama);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        NPM = new JLabel("NPM: "+String.format("%d", mahasiswa.getNpm()));
        NPM.setFont(SistemAkademikGUI.fontGeneral);
        NPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(NPM);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        jurusan = new JLabel("Jurusan: "+mahasiswa.getJurusan());
        jurusan.setFont(SistemAkademikGUI.fontGeneral);
        jurusan.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(jurusan);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        MataKuliah[] matkul = mahasiswa.getMataKuliah();
        if (mahasiswa.getBanyakMatkul() == 0) {
            JLabel belumAda = new JLabel("Belum ada mata kuliah yang diambil.");
            belumAda.setFont(SistemAkademikGUI.fontGeneral);
            belumAda.setAlignmentX(Component.CENTER_ALIGNMENT);
            panel.add(belumAda);
            panel.add(Box.createRigidArea(new Dimension(0,10)));
        }
        daftarMatkul = new JLabel[mahasiswa.getBanyakMatkul()];
        for (int i = 0; i < mahasiswa.getBanyakMatkul(); i++) {
            daftarMatkul[i] = new JLabel(String.format("%d.",i+1)+matkul[i].getNama());
            daftarMatkul[i].setFont(SistemAkademikGUI.fontGeneral);
            daftarMatkul[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            panel.add(daftarMatkul[i]);
            panel.add(Box.createRigidArea(new Dimension(0,10)));
        }
        
        totalSKS = new JLabel("Total SKS: "+String.format("%d", mahasiswa.getTotalSKS()));
        totalSKS.setFont(SistemAkademikGUI.fontGeneral);
        totalSKS.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(totalSKS);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        cekIRS = new JLabel("Hasil Pengecekan IRS:");
        cekIRS.setFont(SistemAkademikGUI.fontGeneral);
        cekIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(cekIRS);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        mahasiswa.cekIRS();
        String[] masalah = mahasiswa.getMasalahIRS();
        if (mahasiswa.getBanyakMasalahIRS() == 0) {
            JLabel tidakMasalah = new JLabel("IRS tidak bermasalah.");
            tidakMasalah.setFont(SistemAkademikGUI.fontGeneral);
            tidakMasalah.setAlignmentX(Component.CENTER_ALIGNMENT);
            panel.add(tidakMasalah);
            panel.add(Box.createRigidArea(new Dimension(0,10)));
        }
        masalahIRS = new JLabel[mahasiswa.getBanyakMasalahIRS()];
        for (int i = 0; i < mahasiswa.getBanyakMasalahIRS(); i++) {
            masalahIRS[i] = new JLabel(masalah[i]);
            masalahIRS[i].setFont(SistemAkademikGUI.fontGeneral);
            masalahIRS[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            panel.add(masalahIRS[i]);
            panel.add(Box.createRigidArea(new Dimension(0,10)));
        }

        selesai = new JButton("Selesai");
        selesai.setFont(SistemAkademikGUI.fontGeneral);
        selesai.setAlignmentX(Component.CENTER_ALIGNMENT);
        selesai.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(selesai);
        panel.add(Box.createRigidArea(new Dimension(0,10)));
        
        frame.add(panel);
        frame.validate();
        frame.repaint();
    }
}
