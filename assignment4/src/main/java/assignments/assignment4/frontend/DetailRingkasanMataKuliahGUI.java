package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    private JPanel panel;
    private JLabel titleLabel;
    private JLabel nama;
    private JLabel kode;
    private JLabel daftarMhs;
    private JLabel[] daftarMatkul;
    private JLabel SKS;
    private JLabel kapasitas;
    private JLabel jumlahMhs;
    private JButton selesai;

    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Detail Ringkasan Mata Kuliah
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(Box.createRigidArea(new Dimension(0,10)));

        titleLabel = new JLabel("Detail Ringkasan Mata Kuliah");
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(titleLabel);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        nama = new JLabel("Nama: "+mataKuliah.getNama());
        nama.setFont(SistemAkademikGUI.fontGeneral);
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(nama);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        kode = new JLabel("Kode: "+mataKuliah.getKode());
        kode.setFont(SistemAkademikGUI.fontGeneral);
        kode.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(kode);
        panel.add(Box.createRigidArea(new Dimension(0,10)));
        
        SKS = new JLabel("SKS: "+String.format("%d", mataKuliah.getSKS()));
        SKS.setFont(SistemAkademikGUI.fontGeneral);
        SKS.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(SKS);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        jumlahMhs = new JLabel("Jumlah mahasiswa: "+String.format("%d", mataKuliah.getJumlahMahasiswa()));
        jumlahMhs.setFont(SistemAkademikGUI.fontGeneral);
        jumlahMhs.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(jumlahMhs);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        kapasitas = new JLabel("Kapasitas: "+String.format("%d",mataKuliah.getKapasitas()));
        kapasitas.setFont(SistemAkademikGUI.fontGeneral);
        kapasitas.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(kapasitas);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        daftarMhs = new JLabel("Daftar Mahasiswa: ");
        daftarMhs.setFont(SistemAkademikGUI.fontGeneral);
        daftarMhs.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(daftarMhs);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        Mahasiswa[] daftarMahasis = mataKuliah.getDaftarMahasiswa();
        if (mataKuliah.getJumlahMahasiswa() == 0) {
            JLabel belumAda = new JLabel("Belum ada mahasiswa yang mengambil mata kuliah ini.");
            belumAda.setFont(SistemAkademikGUI.fontGeneral);
            belumAda.setAlignmentX(Component.CENTER_ALIGNMENT);
            panel.add(belumAda);
            panel.add(Box.createRigidArea(new Dimension(0,10)));
        }
        daftarMatkul = new JLabel[mataKuliah.getJumlahMahasiswa()];
        for (int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++) {
            daftarMatkul[i] = new JLabel(String.format("%d.",i+1)+daftarMahasis[i].getNama());
            daftarMatkul[i].setFont(SistemAkademikGUI.fontGeneral);
            daftarMatkul[i].setAlignmentX(Component.CENTER_ALIGNMENT);
            panel.add(daftarMatkul[i]);
            panel.add(Box.createRigidArea(new Dimension(0,10)));
        }

        selesai = new JButton("Selesai");
        selesai.setFont(SistemAkademikGUI.fontGeneral);
        selesai.setAlignmentX(Component.CENTER_ALIGNMENT);
        selesai.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(selesai);
        panel.add(Box.createRigidArea(new Dimension(0,10)));
        
        frame.add(panel);
        frame.validate();
        frame.repaint();
        
    }
}
