package assignments.assignment4.frontend;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    private JLabel titleLabel;
    private JPanel panel;
    private JButton tambahMahasiswa;
    private JButton tambahMataKuliah;
    private JButton tambahIRS;
    private JButton hapusIRS;
    private JButton lihatRingkasanMahasiswa;
    private JButton lihatRingkasanMataKuliah;
    private JPanel panel2;
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        
        // TODO: Implementasikan Halaman Home
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        panel.add(titleLabel);
        panel.add(Box.createRigidArea(new Dimension(0,10)));


        tambahMahasiswa = new JButton("Tambah Mahasiswa");
        tambahMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
        tambahMahasiswa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(tambahMahasiswa);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        tambahMataKuliah = new JButton("Tambah Mata Kuliah");
        tambahMataKuliah.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahMataKuliah.setFont(SistemAkademikGUI.fontGeneral);
        tambahMataKuliah.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(tambahMataKuliah);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        tambahIRS = new JButton("Tambah IRS");
        tambahIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahIRS.setFont(SistemAkademikGUI.fontGeneral);
        tambahIRS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(tambahIRS);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        hapusIRS = new JButton("Hapus IRS");
        hapusIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        hapusIRS.setFont(SistemAkademikGUI.fontGeneral);
        hapusIRS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(hapusIRS);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        lihatRingkasanMahasiswa = new JButton("Lihat Ringkasan Mahasiswa");
        lihatRingkasanMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        lihatRingkasanMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
        lihatRingkasanMahasiswa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(lihatRingkasanMahasiswa);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        lihatRingkasanMataKuliah = new JButton("Lihat Ringkasan Mata Kuliah");
        lihatRingkasanMataKuliah.setAlignmentX(Component.CENTER_ALIGNMENT);
        lihatRingkasanMataKuliah.setFont(SistemAkademikGUI.fontGeneral);
        lihatRingkasanMataKuliah.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(lihatRingkasanMataKuliah);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        frame.add(panel);
        frame.validate();
        frame.repaint();
    }
}
//
