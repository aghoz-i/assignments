package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {
    private JPanel panel;
    private JLabel titleLabel;
    private JLabel pilihNPM;
    private JComboBox pilihNPMDropdown;
    private JButton lihat;
    private JButton kembali;

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Ringkasan Mahasiswa
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(Box.createRigidArea(new Dimension(0,10)));

        titleLabel = new JLabel("Ringkasan Mahasiswa");
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(titleLabel);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        pilihNPM = new JLabel("Pilih NPM");
        pilihNPM.setFont(SistemAkademikGUI.fontGeneral);
        pilihNPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(pilihNPM);
        panel.add(Box.createRigidArea(new Dimension(0,10)));


        pilihNPMDropdown = new JComboBox(new DefaultComboBoxModel(getDaftarNPMUnefficientlySorted(daftarMahasiswa)));
        pilihNPMDropdown.setFont(SistemAkademikGUI.fontGeneral);
        pilihNPMDropdown.setMaximumSize(new Dimension(200, 20));
        pilihNPMDropdown.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(pilihNPMDropdown);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        lihat = new JButton("Lihat");
        lihat.setFont(SistemAkademikGUI.fontGeneral);
        lihat.setAlignmentX(Component.CENTER_ALIGNMENT);
        lihat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String teksNPM = "" + pilihNPMDropdown.getItemAt(pilihNPMDropdown.getSelectedIndex());
                boolean sudah = false;
                if (teksNPM.equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    sudah = true;
                } 
                if (!sudah) {
                    Mahasiswa mahasiswa = getMahasiswa(Long.parseLong(teksNPM), daftarMahasiswa);
                    frame.remove(panel);
                    frame.validate();
                    new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMahasiswa, daftarMataKuliah);
                }
            }
        });
        panel.add(lihat);
        panel.add(Box.createRigidArea(new Dimension(0,10)));

        kembali = new JButton("Kembali");
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembali.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.remove(panel);
                frame.validate();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        panel.add(kembali);
        panel.add(Box.createRigidArea(new Dimension(0,10)));
        
        frame.add(panel);
        frame.validate();
        frame.repaint();
    }

    private String[] getDaftarNPMUnefficientlySorted(ArrayList<Mahasiswa> daftarMahasiswa) {
        int jumlahMahasiswa = daftarMahasiswa.size();
        if (jumlahMahasiswa == 0) {
            String[] rettt = {""};
            return rettt;
        }
        String[] ret = new String[jumlahMahasiswa];
        for (int i = 0; i < jumlahMahasiswa; i++) {
            Mahasiswa mahasiswa = daftarMahasiswa.get(i);
            ret[i] = String.format("%d", mahasiswa.getNpm());
        }
        for (int i = 0; i < jumlahMahasiswa; i++) {
            for (int j = i+1; j < jumlahMahasiswa; j++) {
                if (ret[i].compareTo(ret[j]) >= 0) {
                    String temp = ret[i];
                    ret[i] = ret[j];
                    ret[j] = temp;
                }
            }
        }
        return ret;
    }
    // Uncomment method di bawah jika diperlukan
    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
