package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem
    
    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */
    public static long getTahunLahir(long npm) {
        return (long)(npm/100)%10000;
    }

    public static int compressDigit(int num) {
        int ret = 0;
        while (num > 0) {
            ret += num % 10;
            num /= 10;
        }
        return ret;
    }

    public static boolean getPerkalianDigit(String npmString) {
        int hasil = 0;
        for (int i = 0 ; i < 7; i++) {
            hasil += (npmString.charAt(i) - '0') * (npmString.charAt(12-i) - '0');
        }
        while (hasil >= 10) hasil = compressDigit(hasil);
        return hasil == npmString.charAt(13) - '0';
    }

    public static int getTahunMasuk(long npm) {
        return (int)(npm / 1000000000000L);
    }

    public static String getTanggal(long npm) {
        String npmString = String.format("%d",npm);
        //     /          Hari        /         /         Bulan        /         /   Tahun   /
        return npmString.substring(4,6) + "-" + npmString.substring(6,8) + "-" + getTahunLahir(npm);
    }

    public static String getKodeJurusan(long npm) {
        String code = String.format("%d",npm).substring(2,4);
        if (code.equals("01")) return "Ilmu Komputer";
        if (code.equals("02")) return "Sistem Informasi";
        if (code.equals("03")) return "Teknologi Informasi";
        if (code.equals("11")) return "Teknik Telekomunikasi";
        if (code.equals("12")) return "Teknik Elektro";
        return "INVALID";
    }

    public static boolean validate(long npm) {
        // TODO: validate NPM, return it with boolean
        String npmString = String.format("%d",npm);
        // Mengecek jumlah digit pada npm
        if (npmString.length() != 14) return false;
        // Mengecek umur mahasiswa pemilik NPM
        if (getTahunMasuk(npm) - getTahunLahir(npm)%100 < 15) return false;
        // Mengecek Kode Jurusan pada NPM
        if (getKodeJurusan(npm) == "INVALID") return false;
        // Mengecek perkalian digit
        if (!getPerkalianDigit(npmString)) return false;
        // NPM valid
        return true;
    }

    public static String extract(long npm) {
        // TODO: Extract information from NPM, return string with given format
        return String.format("Tahun masuk: 20%d\nJurusan: %s\nTanggal Lahir: %s", getTahunMasuk(npm), getKodeJurusan(npm), getTanggal(npm));
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            // TODO: Check validate and extract NPM
            if (validate(npm)) System.out.println(extract(npm));
            else System.out.println("NPM tidak valid!");
        }
        input.close();
    }
}
